# AdminUrlGenerator
Laravel

## Установка:
**composer.json**

```json
{
  "repositories": [
    {
      "type": "git",
      "url": "https://maaxim@bitbucket.org/maaxim/admin-url-generator.git"
    }
  ]
}
```

```json
{
  "require": {
    "maaxim/admin-url-generator": "^1.0"
  }
}
```

## Пример использования:
```php
maaxim\AdminUrlGenerator\UrlGenerator::generate('привет мир');
```
 Вывод:
``privet-mir``
