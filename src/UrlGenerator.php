<?php

namespace maaxim\AdminUrlGenerator;

use Illuminate\Support\Facades\Facade;

/**
 * @see Uri
 */
class UrlGenerator extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Uri';
    }
}
