<?php

namespace maaxim\AdminUrlGenerator;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function register()
    {
        $this->app->bind('Uri', function ($app) {
            return new Uri();
        });
    }

    public function boot()
    {
        //
    }
}
